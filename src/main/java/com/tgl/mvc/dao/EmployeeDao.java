package com.tgl.mvc.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tgl.mvc.mapper.EmployeeRowMapper;
import com.tgl.mvc.model.Employee;

@Repository
public class EmployeeDao {
	private static final Logger LOG = LogManager.getLogger(EmployeeDao.class);

	private static final String INSERT = "INSERT INTO employee (chName, engName, phone, email, height, weight, bmi) VALUES (:chName, :engName, :phone, :email, :height, :weight, :bmi)";

	private static final String DELETE = "DELETE FROM employee WHERE id=:id";

	private static final String UPDATE = "UPDATE employee SET chName=:chName, engName=:engName, phone=:phone, email=:email, height=:height, weight=:weight, bmi=:bmi WHERE id=:id";

	private static final String SELECT = "SELECT id, chName, engName, phone, email, height, weight, bmi FROM employee";

	private static final String Truncate = "truncate table employee";

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public int insert(Employee employee) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		namedParameterJdbcTemplate.update(INSERT, new BeanPropertySqlParameterSource(employee), keyHolder);
		return keyHolder.getKey().intValue();
	}

	@CacheEvict(value = "empFindById", key = "#employeeId")
	public boolean delete(int employeeId) {
		return namedParameterJdbcTemplate.update(DELETE, new MapSqlParameterSource("id", employeeId)) > 0;
	}

	@CachePut(value = "empFindById", key = "#employee.id")
	public Employee update(Employee employee) {
		namedParameterJdbcTemplate.update(UPDATE, new BeanPropertySqlParameterSource(employee));
		return this.findById(employee.getId());
	}

	@Cacheable(value = "empFindById", key = "#employeeId")
	public Employee findById(int employeeId) {
		try {
			return namedParameterJdbcTemplate.queryForObject(SELECT + " WHERE id=:id",
					new MapSqlParameterSource("id", employeeId), new EmployeeRowMapper());
		} catch (EmptyResultDataAccessException e) {
			LOG.error("EmployeeDao findById failed, id: {}, Exception: {}", employeeId, e);
		}
		return null;
	}

	public boolean truncate() {
		int result = namedParameterJdbcTemplate.update(Truncate, new MapSqlParameterSource("", null));
		return result == 0;
	}

	@CacheEvict(value = "empFindById")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = SQLException.class)
	public boolean batchInsert(List<Employee> list) {
		SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(list.toArray());
		int[] updateCounts = namedParameterJdbcTemplate.batchUpdate(INSERT, batch);
		return updateCounts.length > 0;
	}
}
