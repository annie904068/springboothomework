package com.tgl.mvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.tgl.mvc.dao.EmployeeDao;
import com.tgl.mvc.dao.EmployeeDataInputToSql;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.service.EmployeeService;

@EnableCaching
@ComponentScan({"com.tgl.mvc.*"})
@EnableAutoConfiguration(exclude=ErrorMvcAutoConfiguration.class)
@SpringBootApplication
public class WebApplication {
	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);
	}
	
	//先將db清空
	@Component
	@Order(1)
	public class truncate implements CommandLineRunner {
		@Autowired
		private EmployeeService employeeService;
		
	    @Override
	    public void run(String... args) throws Exception {
	    	employeeService.initTable();
	    	System.out.println("truncate success");
	    }
	}
	
	//再將employee那份資料加進去
	@Component
	@Order(2)
	public class inputEmployeeFile implements CommandLineRunner {
		@Autowired
		private EmployeeDao employeeDao;
		
	    @Override
	    public void run(String... args) throws Exception {
	    	EmployeeDataInputToSql empToSql = new EmployeeDataInputToSql();
	    	List<Employee> emp = empToSql.importData(); //讀檔後，回傳employee形成的list
	    	employeeDao.batchInsert(emp); //將所有的資料加進去db
	        System.out.println("employee file input success");
	    }
	}
}
